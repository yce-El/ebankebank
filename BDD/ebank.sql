-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 26 oct. 2017 à 12:42
-- Version du serveur :  10.1.26-MariaDB
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ebank`
--

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomclt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenclt` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id`, `nomclt`, `prenclt`, `created_at`, `updated_at`) VALUES
(2, 'AKPO AGBAN ', 'Combietevi M.', '2017-10-23 15:52:46', '2017-10-23 15:52:46'),
(3, 'AMETEPE', 'Koffi', '2017-10-23 16:12:17', '2017-10-23 16:12:17'),
(4, 'Mahmoud', 'Saliou', '2017-10-23 16:27:31', '2017-10-23 16:27:31'),
(5, 'ABALO', 'Dopévi', '2017-10-25 00:11:06', '2017-10-25 00:11:06');

-- --------------------------------------------------------

--
-- Structure de la table `courants`
--

CREATE TABLE `courants` (
  `id` int(10) UNSIGNED NOT NULL,
  `soldecpte` double(8,2) NOT NULL,
  `decouvert` double(8,2) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `employe_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `courants`
--

INSERT INTO `courants` (`id`, `soldecpte`, `decouvert`, `client_id`, `employe_id`, `created_at`, `updated_at`) VALUES
(1, 49000.00, 4000.00, 3, 2, '2017-10-24 14:18:54', '2017-10-25 00:48:09'),
(2, 97030.00, 3000.00, 4, 1, '2017-10-24 14:22:48', '2017-10-24 20:46:31');

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

CREATE TABLE `employes` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomempl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prenempl` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `employe_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `employes`
--

INSERT INTO `employes` (`id`, `nomempl`, `prenempl`, `employe_id`, `created_at`, `updated_at`) VALUES
(1, 'test33', 'testREdit', NULL, '2017-10-23 17:44:23', '2017-10-23 23:07:32'),
(2, 'Amahvi', 'Kodjogan', 1, '2017-10-23 17:55:59', '2017-10-23 22:59:11'),
(3, 'Djondo', 'AmahviEdit', NULL, '2017-10-23 18:01:00', '2017-10-23 23:07:03');

-- --------------------------------------------------------

--
-- Structure de la table `epargnes`
--

CREATE TABLE `epargnes` (
  `id` int(10) UNSIGNED NOT NULL,
  `soldecpte` double(8,2) NOT NULL,
  `interet` double(8,2) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `employe_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `epargnes`
--

INSERT INTO `epargnes` (`id`, `soldecpte`, `interet`, `client_id`, `employe_id`, `created_at`, `updated_at`) VALUES
(1, 40000.00, 6.00, 4, 1, '2017-10-24 10:50:43', '2017-10-24 20:53:57'),
(2, 20000.00, 3.00, 3, 2, '2017-10-24 11:26:46', '2017-10-25 00:47:24'),
(3, 25000.00, 2.00, 2, 2, '2017-10-24 11:52:25', '2017-10-25 00:41:16'),
(4, 50000.00, 5.00, 5, 2, '2017-10-25 00:11:43', '2017-10-25 00:33:34');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_10_16_184738_create_clients_table', 1),
(4, '2017_10_16_184900_create_employes_table', 2),
(5, '2017_10_16_184934_create_epargnes_table', 2),
(6, '2017_10_16_185052_create_types_table', 3),
(7, '2017_10_16_185129_create_virements_table', 4);

-- --------------------------------------------------------

--
-- Structure de la table `operations`
--

CREATE TABLE `operations` (
  `id` int(10) UNSIGNED NOT NULL,
  `montantop` double(8,2) NOT NULL,
  `employe_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `courant_id` int(10) UNSIGNED DEFAULT NULL,
  `epargne_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `operations`
--

INSERT INTO `operations` (`id`, `montantop`, `employe_id`, `type_id`, `courant_id`, `epargne_id`, `created_at`, `updated_at`) VALUES
(1, 0.00, 2, 2, 1, NULL, '2017-10-24 16:43:13', '2017-10-24 16:43:13'),
(2, 0.00, 1, 2, 1, NULL, '2017-10-24 16:44:22', '2017-10-24 16:44:22'),
(3, 6000.00, 2, 1, 1, NULL, '2017-10-24 16:55:37', '2017-10-24 16:55:37'),
(4, 30000.00, 3, 1, 2, NULL, '2017-10-24 16:56:41', '2017-10-24 16:56:41'),
(5, 15000.00, 2, 1, 1, NULL, '2017-10-24 20:29:52', '2017-10-24 20:29:52'),
(19, 15000.00, 1, 1, 2, NULL, '2017-10-24 20:35:22', '2017-10-24 20:35:22'),
(25, 15000.00, 1, 1, 2, NULL, '2017-10-24 20:39:16', '2017-10-24 20:39:16'),
(26, 13000.00, 2, 1, 2, NULL, '2017-10-24 20:41:22', '2017-10-24 20:41:22'),
(27, 30000.00, 2, 1, 1, NULL, '2017-10-24 20:43:20', '2017-10-24 20:43:20'),
(28, 2000.00, 1, 1, 1, NULL, '2017-10-24 20:44:18', '2017-10-24 20:44:18'),
(29, 12000.00, 2, 1, 2, NULL, '2017-10-24 20:46:31', '2017-10-24 20:46:31'),
(30, 12000.00, 2, 1, 1, NULL, '2017-10-24 20:47:15', '2017-10-24 20:47:15'),
(31, 20000.00, 3, 1, NULL, 2, '2017-10-24 20:53:37', '2017-10-24 20:53:37'),
(32, 30000.00, 2, 1, NULL, 1, '2017-10-24 20:53:57', '2017-10-24 20:53:57'),
(33, 40000.00, 2, 1, NULL, 4, '2017-10-25 00:24:44', '2017-10-25 00:24:44'),
(34, 500.00, 1, 2, 3, NULL, '2017-10-25 00:32:46', '2017-10-25 00:32:46'),
(35, 5000.00, 2, 2, 4, NULL, '2017-10-25 00:33:34', '2017-10-25 00:33:34'),
(36, 5000.00, 2, 1, NULL, 3, '2017-10-25 00:40:45', '2017-10-25 00:40:45'),
(37, 10000.00, 1, 2, NULL, 3, '2017-10-25 00:41:16', '2017-10-25 00:41:16'),
(38, 20000.00, 2, 2, NULL, 2, '2017-10-25 00:47:24', '2017-10-25 00:47:24'),
(42, 20000.00, 2, 2, 1, NULL, '2017-10-25 00:48:09', '2017-10-25 00:48:09');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE `types` (
  `id` int(10) UNSIGNED NOT NULL,
  `libelle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `types`
--

INSERT INTO `types` (`id`, `libelle`, `created_at`, `updated_at`) VALUES
(1, 'Crédit', NULL, NULL),
(2, 'Débit', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Reejoyce', 'akakporeejoyce@gmail.com', '$2y$10$1oWCZoAUtcM5UiaxJxfZcO1zn69GcwMw.mPPIY5lIU7eN6/8L7GtW', 'sU7YXZjDeFREiW0mky3Pb6Y9PvyGnPC49dCXqqzZI1zfebr457MNEvzDYyQi', '2017-10-24 23:14:02', '2017-10-25 00:03:03'),
(2, 'essaie', 'essaie@yahoo.fr', '$2y$10$IDjBi.ZCeUI2CgRy12fHX.h0gkzC9wXE7/.zLOGS2bQEb5aPzAL9m', 'zUUptd9ADZY7MvUrTTuHTSVFEjdT4FCt458N9Wf37XmNEM0tFGHlUGib99sJ', '2017-10-25 00:04:05', '2017-10-25 00:48:32');

-- --------------------------------------------------------

--
-- Structure de la table `virements`
--

CREATE TABLE `virements` (
  `id` int(10) UNSIGNED NOT NULL,
  `montantop` double(8,2) NOT NULL,
  `cptebenef` int(10) UNSIGNED NOT NULL,
  `employe_id` int(10) UNSIGNED NOT NULL,
  `type_id` int(10) UNSIGNED NOT NULL,
  `courant_id` int(10) UNSIGNED NOT NULL,
  `epargne_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `courants`
--
ALTER TABLE `courants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `courants_client_id_foreign` (`client_id`);

--
-- Index pour la table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employes_employe_id_foreign` (`employe_id`);

--
-- Index pour la table `epargnes`
--
ALTER TABLE `epargnes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `epargnes_client_id_foreign` (`client_id`),
  ADD KEY `epargnes_employe_id_foreign` (`employe_id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `operations`
--
ALTER TABLE `operations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operations_employe_id_foreign` (`employe_id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Index pour la table `virements`
--
ALTER TABLE `virements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `virements_employe_id_foreign` (`employe_id`),
  ADD KEY `virements_type_id_foreign` (`type_id`),
  ADD KEY `virements_courant_id_foreign` (`courant_id`),
  ADD KEY `virements_epargne_id_foreign` (`epargne_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `courants`
--
ALTER TABLE `courants`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `employes`
--
ALTER TABLE `employes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `epargnes`
--
ALTER TABLE `epargnes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `operations`
--
ALTER TABLE `operations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT pour la table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `virements`
--
ALTER TABLE `virements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `courants`
--
ALTER TABLE `courants`
  ADD CONSTRAINT `courants_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`);

--
-- Contraintes pour la table `employes`
--
ALTER TABLE `employes`
  ADD CONSTRAINT `employes_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employes` (`id`);

--
-- Contraintes pour la table `epargnes`
--
ALTER TABLE `epargnes`
  ADD CONSTRAINT `epargnes_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `epargnes_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employes` (`id`);

--
-- Contraintes pour la table `operations`
--
ALTER TABLE `operations`
  ADD CONSTRAINT `operations_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employes` (`id`);

--
-- Contraintes pour la table `virements`
--
ALTER TABLE `virements`
  ADD CONSTRAINT `virements_courant_id_foreign` FOREIGN KEY (`courant_id`) REFERENCES `courants` (`id`),
  ADD CONSTRAINT `virements_employe_id_foreign` FOREIGN KEY (`employe_id`) REFERENCES `employes` (`id`),
  ADD CONSTRAINT `virements_epargne_id_foreign` FOREIGN KEY (`epargne_id`) REFERENCES `epargnes` (`id`),
  ADD CONSTRAINT `virements_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
