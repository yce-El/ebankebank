<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Clients extends Model
{
    protected $fillable = ['nomclt', 'prenclt'];

    /*
     * Un client a plusieurs compte courants et epargne
     */
    public function courants(){
    	return $this->hasMany(Courants::class);
    }

    public function epargnes(){
    	return $this->hasMany(Epargnes::class);
    }
}
