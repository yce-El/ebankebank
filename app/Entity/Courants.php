<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Courants extends Model
{
    protected $fillable = ['soldecpte', 'decouvert', 'client_id', 'employe_id'];

    /*
     * Un compte courant appartient a un seul client
     */
    public function client(){
    	return $this->belongsTo(Clients::class);
    }

    /*
     * Un compte courant a été cré par un seul employé
     */
    public function employe(){
    	return $this->belongsTo(Employes::class);
    }

    /*
     * Un compte Courant peut faire l'objet de plusieur opération
     */
    public function operations(){
    	return $this->hasMany(operations::class);
    }

}
