<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Employes extends Model
{
    protected $fillable = ['nomempl', 'prenempl', 'employe_id'];

    /*
     * Un employeur crée plutieurs compte courants et epargne
     */
    public function courants(){
    	return $this->hasMany(Courants::class);
    }

    public function epargnes(){
    	return $this->hasMany(Epargnes::class);
    }

    public function operations(){
    	return $this->hasMany(Operations::class);
    }

    public function parent(){
        return $this->belongsTo(Employes::class, 'employe_id');
    }

    public function childs(){
        return $this->hasMany(Employes::class);
    }

}
