<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Epargnes extends Model
{
    protected $fillable = ['soldecpte', 'interet', 'client_id', 'employe_id'];

     /*
     * Un compte Epargne appartient a un seul client
     */
    public function client(){
    	return $this->belongsTo(Clients::class);
    }

    /*
     * Un compte courant a été cré par un seul employé
     */
    public function employe(){
    	return $this->belongsTo(Employes::class);
    }

    /*
     * Un compte épargne peut faire l'objet de plusieur opération
     */
    public function operations(){
    	return $this->hasMany(operations::class);
    }
}
