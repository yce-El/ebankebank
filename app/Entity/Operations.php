<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Operations extends Model
{
    protected $fillable = ['montantop', 'type_id', 'courant_id', 'epargne_id', 'employe_id'];

    /*
     * Une opération est faites sur un seul compte a la fois
     */
    public function epargne(){
    	return $this->belongsTo(Epargnes::class);
    }

    public function courant(){
    	return $this->belongsTo(Courants::class);
    }

    public function employe(){
    	return $this->belongsTo(Employes::class);
    }

    public function type(){
    	return $this->belongsTo(Types::class);
    }
}
