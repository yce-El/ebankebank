<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Types extends Model
{
    protected $fillable = ['libelle'];

    public function operations(){
    	return $this->hasMany(Operations::class);
    }
    
}
