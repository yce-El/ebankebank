<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Virements extends Model
{
    protected $fillable = ['montantop', 'cptebenef', 'type_id', 'courant_id', 'epargne_id', 'employe_id'];
}
