<?php 

namespace App\Gestions;

interface PhotoGestionInterface
{
  public function save($image);
}