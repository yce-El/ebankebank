<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Entity\Courants;
use App\Entity\Epargnes;
use App\Entity\Employes;
use App\Entity\Operations;
use App\Entity\Clients;

class AccueilController extends Controller
{

	public function getHome()
	{
		$client = Clients::all()->count();
		$cptc = Courants::all()->count();
		$cpte = Epargnes::all()->count();
		$ope = Operations::all()->count();
		return view('home/home', [
									'client' => $client, 
									'cptc' => $cptc, 
									'cpte' => $cpte, 
									'ope' => $ope
									]);
	}
}