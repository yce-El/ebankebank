<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Entity\Clients;
use App\Http\Requests\ClientRequest;
use App\Http\Requests\ClientEditRequest;

class ClientsController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth');
	}
	public function getListe()
	{
		$clients = Clients::all();
		return view('Clients/liste',['clients' => $clients]);
	}

	public function getAdd()
	{
		return view('clients/add');
	}

	public function postAdd(ClientRequest $request)
	{
		$client  = new Clients();
		$client->nomclt = $request->input('nom');
		$client->prenclt = $request->input('prenom');
		$client->save();
		$request->session()->flash('success', 'Le client a été bien enrégistré');
		//Session::put('success', 'Le client a été bien enrégistré');
		return response()->json();
		//return redirect('/clients/liste')->with('success', 'Le client a été bien enrégistré');
	}


	public function getEdit($id)
	{
		$id = (int) $id;
		$client = Clients::findOrFail($id);
		return view('clients/edit',  ['client' => $client]);
	}

	public function postEdit($id, ClientEditRequest $request)
	{
		$id = (int) $id;
		$client = Clients::findOrFail($id);
		$client->nomclt = $request->input('nom');
		$client->prenclt = $request->input('prenom');
		$client->save();
		$request->session()->flash('success', 'Le client a été bien modifié');
		return response()->json();
		
	}

	public function getDel($id)
	{
		$id = (int) $id;
		$client = Clients::findOrFail($id);
		$client->delete();
		session()->flash('success', 'Le client a été supprimé avec le succès');
		return response()->json();
		
	}

}