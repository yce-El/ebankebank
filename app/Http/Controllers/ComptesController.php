<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Courants;
use App\Entity\Epargnes;
use App\Entity\Clients;
use App\Entity\Employes;
use App\Http\Requests\CompteRequest;

class ComptesController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
	public function getListeepargne()
	{
		$comptes = Epargnes::all();

		return view('compte.listeepargne',['comptes' => $comptes]);
	}

	public function getAddepargne()
	{
		$clients = Clients::lists('nomclt', 'id');
		$employes = Employes::lists('nomempl', 'id');
		return view('compte.addepargne',['clients' => $clients, 'employes' => $employes]);
	}

	public function postAddepargne(CompteRequest $request)
	{
		$compte  = new Epargnes();
		$compte->soldecpte = $request->input('solde');
		$compte->interet = $request->input('interet');
		$compte->client_id = $request->input('client');
		$compte->employe_id = $request->input('employe');
		$compte->save();
		$request->session()->flash('success', 'Le compte a été bien enrégistré');
		return response()->json();
	}


	public function getEditepargne($id)
	{
		$id = (int) $id;
		$compte = Epargnes::findOrFail($id);
		$clients = Clients::lists('nomclt', 'id');
		$employes = Employes::lists('nomempl', 'id');

		return view('compte.editepargne',  ['compte' => $compte, 'clients' => $clients, 'employes' => $employes]);
	}

	public function postEditepargne($id, CompteRequest $request)
	{
		//echo "je suis ici"; exit;
		$compte = Epargnes::findOrFail($id);
		$compte->soldecpte = $request->input('solde');
		$compte->interet = $request->input('interet');
		$compte->client_id = $request->input('client');
		$compte->employe_id = $request->input('employe');
		$compte->save();
		$request->session()->flash('success', 'Le compte a été bien modifié');
		return response()->json();
		
	}

	public function getDelepargne($id)
	{
		$id = (int) $id;
		$compte = Epargnes::findOrFail($id);
		$compte->delete();
		session()->flash('success', 'Le compte a été supprimé avec succès');
		return response()->json();
		
	}

}