<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Courants;
use App\Entity\Clients;
use App\Entity\Employes;
use App\Http\Requests\ComptecRequest;

class ComptescourantController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth');
	}

	public function getListe()
	{
		$comptes = Courants::all();

		return view('compte.listec',['comptes' => $comptes]);
	}

	public function getAdd()
	{
		$clients = Clients::lists('nomclt', 'id');
		$employes = Employes::lists('nomempl', 'id');
		return view('compte.addc',['clients' => $clients, 'employes' => $employes]);
	}

	public function postAdd(ComptecRequest $request)
	{
		$compte  = new Courants();
		$compte->soldecpte = $request->input('solde');
		$compte->decouvert = $request->input('decouvert');
		$compte->client_id = $request->input('client');
		$compte->employe_id = $request->input('employe');
		$compte->save();
		$request->session()->flash('success', 'Le compte a été bien enrégistré');
		return response()->json();
	}


	public function getEdit($id)
	{
		$id = (int) $id;
		$compte = Courants::findOrFail($id);
		$clients = Clients::lists('nomclt', 'id');
		$employes = Employes::lists('nomempl', 'id');

		return view('compte.editc',  ['compte' => $compte, 'clients' => $clients, 'employes' => $employes]);
	}

	public function postEdit($id, ComptecRequest $request)
	{
		//echo "je suis ici"; exit;
		$compte = Courants::findOrFail($id);
		$compte->soldecpte = $request->input('solde');
		$compte->decouvert = $request->input('decouvert');
		$compte->client_id = $request->input('client');
		$compte->employe_id = $request->input('employe');
		$compte->save();
		$request->session()->flash('success', 'Le compte a été bien modifié');
		return response()->json();
		
	}

	public function getDel($id)
	{
		$id = (int) $id;
		$compte = Courants::findOrFail($id);
		$compte->delete();
		session()->flash('success', 'Le compte a été supprimé avec succès');
		return response()->json();
		
	}

}