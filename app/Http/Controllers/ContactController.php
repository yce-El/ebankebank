<?php

namespace App\Http\Controllers;


use Mail;
use App\Http\Requests\ContactRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;

class ContactController extends Controller
{

	public function getForm()
	{
		return view('contacts/contact');
	}

	public function postForm(ContactRequest $request)
	{
		Mail::send('contacts/email_contact', $request->all(), function(){
			$message->to('reej@perso.com')
					->subject('Contact');
		});
		return view('contacts/confirm');
	}
}