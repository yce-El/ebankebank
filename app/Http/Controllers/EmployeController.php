<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Entity\Employes;
use App\Http\Requests\ClientRequest;
use App\Http\Requests\ClientEditRequest;

class EmployeController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth');
	}
	public function getListe()
	{
		$employes = Employes::all();
		return view('employes.liste',['employes' => $employes]);
	}

	public function getAdd()
	{
		$employes = Employes::lists('nomempl', 'id');
		return view('employes.add',['employes' => $employes]);
	}

	public function postAdd(ClientRequest $request)
	{
		$employe  = new Employes();
		$employe->nomempl = $request->input('nom');
		$employe->prenempl = $request->input('prenom');
		if($request->input('employe') == null){
			$employe->employe_id = null;
		}
		else{
			$employe->employe_id = $request->input('employe');
		}
		$employe->save();
		$request->session()->flash('success', 'L\'employé a été bien enrégistré');
		return response()->json();
		//return redirect('/clients/liste')->with('success', 'Le employe a été bien enrégistré');
	}


	public function getEdit($id)
	{
		$id = (int) $id;
		$employe = Employes::findOrFail($id); 
		$employes = Employes::lists('nomempl', 'id');
		return view('employes/edit',  ['employe' => $employe, 'employes' => $employes]);
	}

	public function postEdit($id, ClientEditRequest $request)
	{
		$id = (int) $id;
		$employe = Employes::findOrFail($id);
		$employe->nomempl = $request->input('nom');
		$employe->prenempl = $request->input('prenom');
		if($request->input('employe') == null){
			$employe->employe_id = null;
		}
		else{
			$employe->employe_id = $request->input('employe');
		}
		
		$employe->save();
		$request->session()->flash('success', 'L\'employé a été bien modifié');
		return response()->json();
		
	}

	public function getDel($id)
	{
		$id = (int) $id;
		$employe = Employes::findOrFail($id);
		$employe->delete();
		session()->flash('success', 'L\'employé a été supprimé avec le succès');
		return response()->json();
		
	}

}