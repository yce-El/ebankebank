<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Entity\Courants;
use App\Entity\Epargnes;
use App\Entity\Clients;
use App\Entity\Employes;
use App\Entity\Types;
use App\Entity\Operations;
use App\Http\Requests\CompteRequest;

class OperationcController extends Controller
{

	public function __construct()
	{
	    $this->middleware('auth');
	}
	public function getListe($id)
	{
		$compte = Courants::findOrFail($id);
		$operations = Operations::Where('courant_id', $id)->get();
		//var_dump($operations); exit;
		//dd($operations);
		return view('operations.listec',['operations' => $operations, 'compte' => $compte]);
	}

	public function getAdd($id)
	{
		$compte = Courants::findOrFail($id);
		//var_dump($compte); exit;
		$ep = Epargnes::lists('id', 'id');
		$clients = Clients::lists('nomclt', 'id');
		$employes = Employes::lists('nomempl', 'id');
		$types = Types::lists('libelle', 'id');
		return view('operations.addc',['compte' => $compte, 'types' => $types, 'clients' => $clients, 'ep' => $ep, 'employes' => $employes]);
	}

	public function postAdd(Request $request)
	{

		//exit('je suis la');
		$compte_id = $request->input('compte');
		$compte = Courants::findOrFail($compte_id);
		$montant = $request->input('montant');
		$intit = $request->input('types');
		if($intit == 2)
		{
			$reste = $compte->soldecpte - $montant;
			//echo $reste; exit;
			if($reste <= 3000)
			{
				$request->session()->flash('success', 'Le compte est à son seuil critique');
				return response()->json();
			}
			else
			{
				$operation  = new Operations();
				$operation->type_id = $intit;
				$operation->courant_id = $compte_id;
				$operation->epargne_id = NULL;
				$operation->employe_id = $request->input('employe');
				$operation->montantop = $montant;
				$compte->soldecpte = $compte->soldecpte - $montant;
				
				$operation->save();
				$compte->save();
				$request->session()->flash('success', 'Opération effectuée avec succès');
				return response()->json();
			}
		}
		else
		{
			$operation  = new Operations();
			$operation->type_id = $intit;
			$operation->courant_id = $compte_id;
			$operation->epargne_id = NULL;
			$operation->employe_id = $request->input('employe');
			$operation->montantop = $montant;
			



			$compte->soldecpte = $compte->soldecpte + $montant;
			
			$operation->save();
			$compte->save();
			$request->session()->flash('success', 'Opération effectuée avec succès');
			return response()->json();
		}

		
	}


}