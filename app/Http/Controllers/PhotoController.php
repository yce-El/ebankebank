<?php

namespace App\Http\Controllers;
use App\Http\Requests\ImageRequest;
use App\Http\Controllers\Controller;
use App\Gestions\PhotoGestion;


class PhotoController extends Controller
{

	private $request;
	private $photoG;

	public function __construct(PhotoGestion $gestion)
	{
		$this->photoG = $gestion;
	}
	public function getForm()
	{
		return view('photo/image');
	}

	public function postForm(ImageRequest $request)
	{
		$image = $request->file('image');
		if($this->photoG->save($image))
		{
			return view('photo/confirm');
		}
		return view('photo/image');
	}
}