<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompteRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'solde' => 'required|integer|min:3000',
            'interet' => 'required|integer|min:2',
            'client' => 'required|integer',
            'employe' => 'required|integer'
        ];
    }
}
