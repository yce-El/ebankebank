<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::controller('clients', 'ClientsController');

Route::controller('employes', 'EmployeController');

Route::controller('comptes', 'ComptesController');

Route::controller('comptesc', 'ComptescourantController');

Route::controller('operationse', 'OperationeController');
Route::controller('operationsc', 'OperationcController');

Route::get('/', [
				'as' => 'home',
				'uses' => 'AccueilController@getHome'
	] )->middleware('auth');



Route::auth();

Route::get('/home', 'HomeController@index');
