<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courants', function (Blueprint $table) {
            $table->increments('id');
            $table->float('soldecpte');
            $table->float('decouvert');
            $table->integer('client_id')->unsigned();
            $table->integer('employe_id')->unsigned();
            $table->timestamps();

            $table->foreign('client_id')->references ('id')->on('clients')
                            ->onDelete('restrict')
                            ->onUpdate('restrict');
            $table->foreign('employe_id')->references ('id')->on('employes')
                            ->onDelete('restrict')
                            ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courants');
    }
}
