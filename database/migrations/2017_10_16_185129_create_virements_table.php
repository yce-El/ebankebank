<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('virements', function (Blueprint $table) {
            $table->increments('id');
            $table->float('montantop');
            $table->integer('cptebenef')->unsigned();
            $table->integer('employe_id')->unsigned();
            $table->integer('type_id')->unsigned();
            $table->integer('courant_id')->unsigned();
            $table->integer('epargne_id')->unsigned();
            $table->timestamps();

            $table->foreign('employe_id')->references('id')->on('employes')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('type_id')->references('id')->on('types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('courant_id')->references('id')->on('courants')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('epargne_id')->references('id')->on('epargnes')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('virements');
    }
}
