<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">x</button>
    <h4 class="modal-title">Modification de <strong>{{ $client->prenclt }}</strong>  {{ $client->nomclt }}</h4>
	</div>


	<div class="modal-body">
	    {!! Form::open(['url' => [URL::to('/clients/edit', $client->id)], 'id' => 'form']) !!}
					

					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">
						{!! Form::text('nom', $client->nomclt, ['class' => 'form-control', 'placeholder' => 'Votre nom']) !!}
						{!! $errors->first('nom', '<small class="help-block">:message</small>') !!}
						
					</div>

					<div class="form-group {!! $errors->has('prenom') ? 'has-error' : '' !!}">
						{!! Form::text('prenom', $client->prenclt, ['class' => 'form-control', 'placeholder' => 'Votre prenom']) !!}
					</div>
					{!! Form::submit('Modifier !', ['class' => 'btn btn-info pull-right']) !!}
				<br/><br/>
	</div>
	</div>
	<div class="modal-footer">
	
		
	</div>
	{!! Form::close() !!}