<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">x</button>
    <h4 class="modal-title">Ajout de compte Epargne</h4>
	</div>
	<div class="modal-body">
	    {!! Form::open(['url' => 'comptes/addepargne', 'id' => 'form']) !!}
					

					<div class="form-group {!! $errors->has('solde') ? 'has-error' : '' !!}">
						{!! Form::text('interet', null, ['class' => 'form-control', 'placeholder' => 'Le taux d\'interêt']) !!}
						{!! $errors->first('interet', '<small class="help-block">:message</small>') !!}

					</div>

					<div class="form-group {!! $errors->has('solde') ? 'has-error' : '' !!}">
						{!! Form::text('solde', null, ['class' => 'form-control', 'placeholder' => 'Votre solde']) !!}
						{!! $errors->first('solde', '<small class="help-block">:message</small>') !!}

					</div>
					<div class="form-group {!! $errors->has('employe') ? 'has-error' : '' !!}">
						
						{{ Form::select('client', array('' => 'Choisir le client') + $clients->toArray(),old('client'), ['class' => 'form-control']) }}

					</div>
					<div class="form-group {!! $errors->has('employe') ? 'has-error' : '' !!}">
						
						{{ Form::select('employe', array('' => 'L\'employé en charge') + $employes->toArray(),old('employe'), ['class' => 'form-control']) }}

					</div>
					
					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
				<br/><br/>
		{!! Form::close() !!}
	</div>
	</div>
	<div class="modal-footer">
	
		
	</div>
	