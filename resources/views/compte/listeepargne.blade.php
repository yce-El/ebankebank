@extends('template')

@section('contenu')
	<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
        Comptes epargnes <small>liste </small>
        </h1>
        <!--  <ol class="breadcrumb">
        <li class="active">
        <i class="fa fa-dashboard"></i>Dashboard / Liste des comptes
        </li>
        </ol> -->
    </div>
</div>
<!-- /.row -->



<div class="row">
    <div class="col-md-12">

        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left" style="padding-top: 7.5px; margin-right: 12px;">Liste des comptes</h4>
                <div class="input-group">
                    <div class="input-group-btn">
                    <a data-toggle="modal" href="{{ URL::to('/comptes/addepargne') }}" data-target="#add" class="btn btn-primary"><i  class="glyphicon glyphicon-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Numéro du compte</th>
                <th>Solde</th>
                <th>interêt</th>
                <th>Propriétaire</th>
                <th>Actions</th>
            </tr>
        </thead>
       
        <tbody>
            @foreach($comptes as $compte)
                <tr>
                    <td>{{ $compte->id }}</td>
                    <td>{{ $compte->soldecpte }} Fcfa</td>
                    <td>{{ $compte->interet }} %</td>
                    <td>{{ $compte->client->prenclt }} {{ $compte->client->nomclt }}</td>
                    <td>  
                        <a data-toggle="modal" data-target="#edit{{ $compte->id }}" href="{{ URL::to('/comptes/editepargne', $compte->id) }}" class="btn btn-xs btn-info">Modifier</a>
                        <!-- <button data-target="{{ $compte->id }}" class="del btn btn-xs btn-danger">Supprimer</button> -->
                         <button data-toggle="modal" data-target="#ope{{ $compte->id }}" href="{{ URL::to('/operationse/add', $compte->id) }}" class="btn btn-xs btn-default">Opérations</button>
                         <a href="{{ URL::to('/operationse/liste', $compte->id) }}" class="btn btn-xs btn-default">Description des opérations</a>
                    </td>
                </tr>

                <div class="modal fade" data-backdrop="false" id="edit{{ $compte->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                        </div>
                    </div>
                </div>

                <div class="modal fade" data-backdrop="false" id="ope{{ $compte->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                        </div>
                    </div>
                </div>

            @endforeach
            </tbody>
            </table>
            
            </div>
        </div>

    </div>

</div>



<div class="modal fade" data-backdrop="false" id="add">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>


@endsection

@section('script')

<script>
    
    $(function(){
            var url = "{{ URL::to('/comptes/listeepargne') }}",
                delUrl = "{{ URL::to('/comptes/delepargne') }}";
            $(document).on('submit', '#form', function(e){
                e.preventDefault();

                $.ajax({
                            method: $(this).attr('method'),
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            dataType: "json"
                        })
                        .done(function(data) {
                            $('.alert-success').removeClass('hidden');

                            $(".modal").on('shown.bs.modal', function(){
                                $('#'+$(this).attr('id')).modal('hide');
                            });
                            
                            
                            document.location.replace(url);
                        })
                        .fail(function(data) {
                            $.each(data.responseJSON, function (key, value) {
                                var input = '#form input[name=' + key + ']';
                                $(input + '+small').text(value);
                                $(input).parent().addClass('has-error');
                            });
                        });
            });

            $('.del').on('click', function(e){
                e.preventDefault();
                var id = $(this).attr('data-target');

                swal({
                      title: 'Suppression !',
                      text: "Etes-vous sûr de vouloir supprimer ?",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Oui !'
                    }, function () {
                        
                         $.ajax({
                            method: 'GET',
                            url: delUrl+'/'+id,
                            dataType: "json"
                        })
                        .done(function(data) {
                            
                            document.location.replace(url);
                        })
                        .fail(function(data) {
                            
                        });
                    })

            });
    });


</script>

@endsection