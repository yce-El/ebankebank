<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">x</button>
    <h4 class="modal-title">Ajout d'un employé</h4>
	</div>
	<div class="modal-body">
	    {!! Form::open(['url' => 'employes/add', 'id' => 'form']) !!}
					

					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">
						{!! Form::text('nom', null, ['class' => 'form-control', 'placeholder' => 'Votre nom']) !!}
						{!! $errors->first('nom', '<small class="help-block">:message</small>') !!}
						
					</div>

					<div class="form-group {!! $errors->has('prenom') ? 'has-error' : '' !!}">
						{!! Form::text('prenom', null, ['class' => 'form-control', 'placeholder' => 'Votre prenom']) !!}
						{!! $errors->first('prenom', '<small class="help-block">:message</small>') !!}

					</div>
					<div class="form-group {!! $errors->has('employe') ? 'has-error' : '' !!}">
						
						{{ Form::select('employe', array('' => 'Choisir un supérieur') + $employes->toArray(),old('employe_id'), ['class' => 'form-control']) }}

					</div>
					
					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
				<br/><br/>
	</div>
	</div>
	<div class="modal-footer">
	
		
	</div>
	{!! Form::close() !!}