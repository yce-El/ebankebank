<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">x</button>
    <h4 class="modal-title">Modification de <strong>{{ $employe->nomempl }}</strong>  {{ $employe->prenempl }}</h4>
	</div>


	<div class="modal-body">
	    {!! Form::open(['url' => [URL::to('/employes/edit', $employe->id)], 'id' => 'form']) !!}
					
					<div class="form-group {!! $errors->has('nom') ? 'has-error' : '' !!}">
						{!! Form::text('nom', $employe->nomempl, ['class' => 'form-control', 'placeholder' => 'Votre nom']) !!}
						{!! $errors->first('nom', '<small class="help-block">:message</small>') !!}
						
					</div>

					<div class="form-group {!! $errors->has('prenom') ? 'has-error' : '' !!}">
						{!! Form::text('prenom', $employe->prenempl, ['class' => 'form-control', 'placeholder' => 'Votre prenom']) !!}
						{!! $errors->first('prenom', '<small class="help-block">:message</small>') !!}

					</div>
					<div class="form-group {!! $errors->has('employe') ? 'has-error' : '' !!}">
						
						{{ Form::select('employe', array('' => 'Choisir un supérieur') + $employes->toArray(),old('employe_id'), ['class' => 'form-control']) }}

					</div>
					{!! Form::submit('Modifier !', ['class' => 'btn btn-info pull-right']) !!}
				<br/><br/>
	</div>
	</div>
	<div class="modal-footer">
	
		
	</div>
	{!! Form::close() !!}