@extends('template')

@section('contenu')
	<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
        Employés <small>liste </small>
        </h1>
    </div>
</div>
<!-- /.row -->



<div class="row">
    <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left" style="padding-top: 7.5px; margin-right: 12px;">Liste des employés</h4>
                <div class="input-group">
                    <div class="input-group-btn">
                    <a data-toggle="modal" href="{{ URL::to('/employes/add') }}" data-target="#add" class="btn btn-primary"><i  class="glyphicon glyphicon-plus"></i></a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Prénom</th>
                <th>Supérieur hiérarchique</th>
                <th>Actions</th>
            </tr>
        </thead>
       
        <tbody>
            @foreach($employes as $employe)
                <tr>
                    <td>{{ $employe->nomempl }}</td>
                    <td>{{ $employe->prenempl }}</td>
                    <td>
                    @if($employe->parent)
                         {{ $employe->parent->prenempl }} {{ $employe->parent->nomempl }}
                    @endif
                    </td>
                    <td>  
                        <a data-toggle="modal" data-target="#edit{{ $employe->id }}" href="{{ URL::to('/employes/edit', $employe->id) }}" class="btn btn-xs btn-info">Modifier</a>
                        <button data-target="{{ $employe->id }}" class="del btn btn-xs btn-danger">Supprimer</button>
                        
                    </td>
                </tr>

                <div class="modal fade" data-backdrop="false" id="edit{{ $employe->id }}">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            
                        </div>
                    </div>
                </div>
            @endforeach
            </tbody>
            </table>
            
            </div>
        </div>

    </div>

</div>



<div class="modal fade" data-backdrop="false" id="add">
    <div class="modal-dialog">
        <div class="modal-content">
            
        </div>
    </div>
</div>



@endsection

@section('script')

<script>
    
    $(function(){
            var url = "{{ URL::to('/employes/liste') }}",
                delUrl = "{{ URL::to('/employes/del') }}";
            $(document).on('submit', '#form', function(e){
                e.preventDefault();

                $.ajax({
                            method: $(this).attr('method'),
                            url: $(this).attr('action'),
                            data: $(this).serialize(),
                            dataType: "json"
                        })
                        .done(function(data) {
                            $('.alert-success').removeClass('hidden');

                            $(".modal").on('shown.bs.modal', function(){
                                $('#'+$(this).attr('id')).modal('hide');
                            });
                            
                            
                            document.location.replace(url);
                        })
                        .fail(function(data) {
                            $.each(data.responseJSON, function (key, value) {
                                var input = '#form input[name=' + key + ']';
                                $(input + '+small').text(value);
                                $(input).parent().addClass('has-error');
                            });
                        });
            });

            $('.del').on('click', function(e){
                e.preventDefault();
                var id = $(this).attr('data-target');

                swal({
                      title: 'Suppression !',
                      text: "Souhaitez-vous vraiment supprimer ?",
                      type: 'warning',
                      showCancelButton: true,
                      confirmButtonColor: '#3085d6',
                      cancelButtonColor: '#d33',
                      confirmButtonText: 'Oui'
                    }, function () {
                        
                         $.ajax({
                            method: 'GET',
                            url: delUrl+'/'+id,
                            dataType: "json"
                        })
                        .done(function(data) {
                            
                            document.location.replace(url);
                        })
                        .fail(function(data) {
                            
                        });
                    })

            });
    });


</script>

@endsection