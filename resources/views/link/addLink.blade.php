@extends('template');

@section('title')
	ShareItBB Add link
@endsection


@section('contenu')
	<form action="{{ route('valid') }}" method="post">
		<div class="form-group">
			<label for="name">Name</label>
			<input class="form-control" type="text" name="name" id="name" placeholder="set a name">
		</div>

		<div class="form-group">
			<label for="link">Link</label>
			<input class="form-control" type="text" name="link" id="link" placeholder="give a link">
		</div>

		<div class="form-group">
			<label for="description">Description</label>

			<textarea class="form-control" placeholder="give description" name="description" id="description" ></textarea>
			
		</div>
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="form-group">
			<button type="submit" class="btn btn-info"> Add my Link </button>
		</div>
	</form>
@endsection