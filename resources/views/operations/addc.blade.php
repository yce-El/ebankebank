<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">x</button>
    <h4 class="modal-title">Opération sur le compte <strong>Courant </strong> de 
    <strong>{{ $compte->client->nomclt }} {{ $compte->client->prenclt}}</strong>

		
    </h4>
	</div>
		

	<div class="modal-body">
		 {!! Form::open(['url' => [URL::to('/operationsc/add', $compte->id)], 'id' => 'form']) !!}
					
					
					<div class="form-group {!! $errors->has('montant') ? 'has-error' : '' !!}">
						{!! Form::text('montant', null, ['class' => 'form-control', 'placeholder' => 'Votre montant']) !!}
						{!! $errors->first('montant', '<small class="help-block">:message</small>') !!}

					</div>

					<div class="form-group {!! $errors->has('typeop') ? 'has-error' : '' !!}">
						{{ Form::select('types', array('' => 'Choisir un type') + $types->toArray(),old('types'), ['class' => 'form-control']) }}
						
					</div>
					<div class="form-group {!! $errors->has('client') ? 'has-error' : '' !!}">
						
						{!! Form::text('client', $compte->client->id, ['class' => 'form-control hidden', 'placeholder' => 'client']) !!}

					</div>
					
					<div class="form-group {!! $errors->has('employe') ? 'has-error' : '' !!}">
						
						{{ Form::select('employe', array('' => 'L\'employé en charge') + $employes->toArray(),old('employe'), ['class' => 'form-control']) }}

					</div>
					
					<div class="form-group {!! $errors->has('compte') ? 'has-error' : '' !!}">
						
						{!! Form::text('compte', $compte->id, ['class' => 'form-control hidden', 'placeholder' => 'Votre montant']) !!}

					</div>
					
					{!! Form::submit('Envoyer !', ['class' => 'btn btn-info pull-right']) !!}
				<br/><br/>
		{!! Form::close() !!}
	</div>
	</div>
	<div class="modal-footer">
	
		
	</div>
	