@extends('template')

@section('contenu')
	<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
        Opérations <small>liste </small>
        </h1>
        <!--  <ol class="breadcrumb">
        <li class="active">
        <i class="fa fa-dashboard"></i>Dashboard / Liste des comptes
        </li>
        </ol> -->
    </div>
</div>
<!-- /.row -->



<div class="row">
    <div class="col-md-12 col-lg-8 col-lg-offset-2">

        <div class="panel panel-default">
            <div class="panel-heading clearfix">
                <h4 class="panel-title pull-left" style="padding-top: 7.5px; margin-right: 12px;">Liste des Opérations sur le compte 

                    <strong>Compte courant</strong> 
                de <strong>{{ $compte->client->prenclt }} {{ $compte->client->nomclt  }}</strong></h4>
                
            </div>
            <div class="panel-body">

               	@foreach($operations as $operation)
					@if($operation->type->id == 2)
						<div class="alert alert-warning">
						  <strong>{{ $operation->type->libelle }}!</strong> Le Compte a été débité de  
                          <strong>{{ $operation->montantop }} F cfa</strong> par
                          <strong>{{ $operation->employe->prenempl }}  {{ $operation->employe->nomempl }}</strong> .
					</div>
					@else
						<div class="alert alert-success">
						<strong>{{ $operation->type->libelle }}!</strong> Le Compte a été débité de  
                          <strong>{{ $operation->montantop }} F cfa</strong> par
                          <strong>{{ $operation->employe->prenempl }}  {{ $operation->employe->nomempl }}</strong> .
					</div>
					@endif
                	
            	@endforeach
            <a href="{{ URL::to('comptesc/liste') }}" class="btn btn-primary">Retour</a>
            </div>
        </div>

    </div>

</div>



@endsection