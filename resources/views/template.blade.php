<!DOCTYPE html>
<html lang="fr">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Coopec Zondo</title>

    <!-- Bootstrap Core CSS -->

    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/jquery.dataTables.min.css') !!}
    {!! Html::style('css/dataTables.bootstrap.min.css') !!}
    {!! Html::style('css/buttons.bootstrap.min.css') !!}
    
    {!! Html::style('css/sb-admin.css') !!}
    {!! Html::style('css/plugins/morris.css') !!}
    {!! Html::style('font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('css/sweetalert.css') !!}


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{ URL::to('/') }}">Coopec Zondo</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{ Auth::user()->name }}  <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        
                        <li>
                            <a href="{{ URL::to('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="{{ URL::to('/') }}"><i class="fa fa-fw fa-dashboard"></i> Acceuil</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-users"></i> Gestion des clients <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="{{ URL::to('/clients/liste') }}"><i class="fa fa-fw fa-table"></i> Liste des clients</a>
                            </li>
                            
                        </ul>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#compte"><i class="fa fa-fw fa-arrows-v"></i> Gestion des Comptes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="compte" class="collapse">
                            <li>
                                <a href="{{ URL::to('/comptes/listeepargne') }}"><i class="fa fa-fw fa-table"></i>Epargnes</a>
                            </li>
                            <li>
                                <a href="{{ URL::to('/comptesc/liste') }}"><i class="fa fa-fw fa-table"></i>Courants</a>
                            </li>
                    
                        </ul>
                    </li>

                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#empl"><i class="fa fa-fw fa-arrows-v"></i> Gestion des employés<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="empl" class="collapse">
                            <li>
                                <a href="{{ URL::to('/employes/liste') }}"><i class="fa fa-fw fa-table"></i> Liste des employés</a>
                            </li>
                    
                        </ul>
                    </li>
                     <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#type"><i class="fa fa-fw fa-arrows-v"></i> Paramètres <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="type" class="collapse">
                            <li>
                                <a href="{{ URL::to('/register') }}"><i class="fa fa-fw fa-user"></i> Ajout d'utilisateurs</a>
                            </li>
                    
                        </ul>
                    </li>
                    <!-- <li>
                        <a href="tables.html"><i class="fa fa-fw fa-table"></i> Tables</a>
                    </li> -->
                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper" style="margin-top: -20px">

            <div class="container-fluid">

                @yield('contenu')

                <p class="small">Développé par AKAKPO AGBAN Combietevi</p>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->

    {!! Html::script('js/jquery.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/jquery.dataTables.min.js') !!}
    {!! Html::script('js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('js/dataTables.buttons.min.js') !!}
    {!! Html::script('js/buttons.bootstrap.min.js') !!}
    {!! Html::script('js/buttons.html5.min.js') !!}
    {!! Html::script('js/buttons.print.min.js') !!}
    {!! Html::script('js/buttons.colVis.min.js') !!}
    {!! Html::script('js/custom.js') !!}
    {!! Html::script('js/sweetalert.min.js') !!}
<script>
	
	$(function(){
            dataTable();
            @if(session()->has('success'))
                var success = '{!! session('success') !!}';
                sweetAlert(success);

            @endif
    });

    
</script>


@yield('script')
    



</body>

</html>
